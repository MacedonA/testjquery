﻿using System;

namespace TestJQuery.Filter
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true)]
    public class FilterableFieldAttribute : Attribute
    {
        public string FilterName { get; set; }

        public FilterableFieldAttribute(string filterName)
        {
            FilterName = filterName;
        }
    }
}