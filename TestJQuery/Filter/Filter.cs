﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace TestJQuery.Filter
{
    public abstract class FilterBase
    {
        protected static readonly Dictionary<Type, Dictionary<string, PropertyInfo>> CachedProperties = new Dictionary<Type, Dictionary<string, PropertyInfo>>();
    }

    public class Filter<T> : FilterBase
    {
        public IList<FilterField> Fields { get; set; }

        public bool IsMatch(T value)
        {
            if (Fields == null || Fields.Count == 0)
                return true;
            return Fields.All(f => f.IsMatch(GetFieldValue(value, f.Key)));
        }

        private static object GetFieldValue(T value, string filterField)
        {
            var key = typeof(T);
            if (!CachedProperties.ContainsKey(key))
            {
                var properties = key
                    .GetProperties()
                    .Where(p => Attribute.IsDefined(p, typeof(FilterableFieldAttribute)))
                    .ToArray();
                var filterableFields = new Dictionary<string, PropertyInfo>(new StringComparerIgnoreCase());
                foreach (var p in properties)
                    foreach (var a in p.GetCustomAttributes(typeof(FilterableFieldAttribute)).OfType<FilterableFieldAttribute>())
                        if (!filterableFields.ContainsKey(a.FilterName))
                            filterableFields.Add(a.FilterName, p);
                CachedProperties.Add(key, filterableFields);

            }
            var fields = CachedProperties[key];
            return fields.ContainsKey(filterField) ? fields[filterField].GetValue(value) : null;
        }

        private class StringComparerIgnoreCase : IEqualityComparer<string>
        {
            public bool Equals(string x, string y)
            {
                return string.Equals(x, y, StringComparison.OrdinalIgnoreCase);
            }

            public int GetHashCode(string obj)
            {
                return obj.ToLower().GetHashCode();
            }
        }
    }

}