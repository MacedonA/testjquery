﻿namespace TestJQuery.Filter
{
    public enum FilterTypeEnum
    {
        Contains = 1,
        Equals = 2,
        GreaterOrEquals = 3,
        LessOrEquals = 4
    }
}