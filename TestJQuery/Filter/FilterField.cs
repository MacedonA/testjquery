﻿using System;

namespace TestJQuery.Filter
{
    public class FilterField
    {
        public string Key { get; set; }

        public FieldTypeEnum FieldType { get; set; }

        public FilterTypeEnum FilterType { get; set; }

        public string Value { get; set; }

        public bool IsMatch(object dataToCompare)
        {
            if (string.IsNullOrEmpty(Value))
                return true;
            switch (FieldType)
            {
                case FieldTypeEnum.Text:
                    var stringValue = dataToCompare?.ToString();
                    if (string.IsNullOrEmpty(stringValue))
                        return false;
                    switch (FilterType)
                    {
                        case FilterTypeEnum.Contains:
                            return stringValue.Contains(Value);
                        case FilterTypeEnum.Equals:
                            return string.Equals(Value, stringValue, StringComparison.OrdinalIgnoreCase);
                        default:
                            return true;
                    }
                case FieldTypeEnum.Date:
                    DateTime expectDate;
                    if (!DateTime.TryParse(Value, out expectDate))
                        return true;
                    var dateValue = dataToCompare as DateTime?;
                    if (!dateValue.HasValue)
                        return false;
                    switch (FilterType)
                    {
                        case FilterTypeEnum.GreaterOrEquals:
                            return dateValue.Value >= expectDate;
                        case FilterTypeEnum.LessOrEquals:
                            return dateValue.Value <= expectDate;
                        case FilterTypeEnum.Equals:
                            return dateValue.Value == expectDate;
                        default:
                            return true;
                    }
                case FieldTypeEnum.Number:
                    double expectNumber;
                    if (!double.TryParse(Value, out expectNumber))
                        return true;
                    var numberValue = dataToCompare as double?;
                    if (!numberValue.HasValue)
                    {
                        numberValue = dataToCompare as int?;
                        if (!numberValue.HasValue)
                            return false;
                    }
                    switch (FilterType)
                    {
                        case FilterTypeEnum.GreaterOrEquals:
                            return numberValue.Value >= expectNumber;
                        case FilterTypeEnum.LessOrEquals:
                            return numberValue.Value <= expectNumber;
                        case FilterTypeEnum.Equals:
                            return Math.Abs(numberValue.Value - expectNumber) < 1E-15;
                        default:
                            return true;
                    }

            }
            return true;
        }
    }
}