﻿namespace TestJQuery.Filter
{
    public enum FieldTypeEnum
    {
        Text = 1,
        Date = 2,
        Number = 3
    }
}