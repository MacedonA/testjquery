﻿var FilterTypeEnum = {
    Contains: 1,
    Equals: 2,
    GreaterOrEquals: 3,
    LessOrEquals: 4
};

var FieldTypeEnum = {
    Text: 1,
    Date: 2,
    Number: 3
};

function FilterBuilder(contentId, fields, onFilterRefresh) {
    this.Fields = fields;
    this.ContentId = contentId;
    this.OnFilterRefresh = onFilterRefresh;

    this.buildContent();
}

FilterBuilder.prototype = {
    GetFilterData : function() {
        var result = [];
        this.Fields.forEach(function (field) {
            if (field.Value !== null && field.Value !== '') {
                result.push({ Key: field.Key, Value: field.Value, FieldType: field.FieldType, FilterType: field.FilterType });
            }
        });
        return { Fields: result };
    },

    IsMatchFilter: function (item) {
        var result = true;
        this.Fields.forEach(function(field) {
            result = result && field.IsMatchFilter(item);
        });
        return result;
    },

    SetStateForControls: function (isEnabled) {
        if (isEnabled !== false) {
            this.setFilterButtonStates();
        } else {
            $('#' + this.ContentId + ' > div#filterButtons > button').prop('disabled', true);
        }
        $('#' + this.ContentId + ' > table > tbody > tr > td > input, #' + this.ContentId + ' > table > tbody > tr > td > select').prop('disabled', isEnabled === false);
    },

    buildContent: function () {
        var contentElement = $('#' + this.ContentId);
        contentElement.html('...loading');

        //Create title.
        var titleSpan = jQuery('<label />').text('Filter By:');

        //Create fields.
        var filterTable = jQuery('<table />');
        var filterBody = jQuery('<tbody />');
        filterTable.append(filterBody);
        var self = this;
        this.Fields.forEach(function (field) {
            filterBody.append(self.createFilterRow(field));
        });

        //Create buttons.
        var buttonsContainer = jQuery('<div />').prop('id', 'filterButtons');
        buttonsContainer.append(jQuery('<button />').addClass('btn btn-default').prop('id', 'applyFilterButton').text('Apply'));
        buttonsContainer.append(jQuery('<button />').addClass('btn btn-default').prop('id', 'clearFilterButton').text('Clear'));

        //Show created elements.
        contentElement.html('');
        contentElement.addClass('form-group');
        contentElement.append(titleSpan);
        contentElement.append(jQuery('<br />'));
        contentElement.append(filterTable);
        contentElement.append(buttonsContainer);

        //Add date pickers.
        $('#' + this.ContentId + ' input[isDate]').each(function() {
            $(this).removeAttr('isDate');
            $(this).datepicker();
            $(this).datepicker('option', 'dateFormat', 'yy-mm-dd');
        });

        //Enable/disable apply button by field values.
        $('#' + this.ContentId + ' input[filterKey]').on('change paste keyup', function () {
            self.setFilterButtonStates();
        });
        $('#' + this.ContentId + ' select[filterKey]').change(function () {
            self.setFilterButtonStates();
        });
        self.setFilterButtonStates();

        //Add button click handlers.
        $('#' + this.ContentId + ' button#applyFilterButton').click(function () {
            self.applyFilter(false);
        });
        $('#' + this.ContentId + ' button#clearFilterButton').click(function () {
            self.applyFilter(true);
        });
    },

    createFilterRow: function(field) {
        var resultRow = jQuery('<tr />');
        resultRow.append(jQuery('<td />').text(field.Description));

        //Create control to filter.
        var filterRowControl = null;
        switch (field.FieldType) {
            case FieldTypeEnum.Date:
                filterRowControl = jQuery('<input />').addClass('form-control').prop('name', field.Key).attr('filterKey', field.Key).attr('isDate', 'true');
                break;
            case FieldTypeEnum.Number:
                filterRowControl = jQuery('<input />').addClass('form-control').prop('name', field.Key).attr('filterKey', field.Key).prop('type', 'number');
                break;
            case FieldTypeEnum.Text:
                if (field.ValuesList === null || field.ValuesList === undefined || field.ValuesList.length === 0) {
                    filterRowControl = jQuery('<input />').addClass('form-control').prop('name', field.Key).attr('filterKey', field.Key);
                } else {
                    filterRowControl = jQuery('<select />').addClass('form-control').prop('name', field.Key).attr('filterKey', field.Key);
                    filterRowControl.append(jQuery('<option />').prop('value', '').text(''));
                    field.ValuesList.forEach(function (value) {
                        filterRowControl.append(jQuery('<option />').prop('value', value).text(value));
                    });
                }
            default:
                break;
        }

        if (filterRowControl !== null && filterRowControl !== undefined) {
            resultRow.append(jQuery('<td />').append(filterRowControl));
        } else {
            resultRow.append(jQuery('<td />'));
        }

        return resultRow;
    },

    applyFilter: function (performClear) {
        var self = this;
        var hasChanges = false;
        this.Fields.forEach(function (field) {
            var previousValue = field.Value;
            var inputForField = $('#' + self.ContentId + ' input[filterKey="' + field.Key + '"]');
            var selectForField = $('#' + self.ContentId + ' select[filterKey="' + field.Key + '"]');
            if (inputForField.length === 1) {
                if (performClear === true) {
                    inputForField.val('');
                }
                field.Value = inputForField.val();
            } else if (selectForField.length === 1) {
                if (performClear === true) {
                    selectForField.val('');
                }
                field.Value = selectForField.val();
            } else {
                field.Value = '';
            }
            if (previousValue !== field.Value) {
                hasChanges = true;
            }
        });
        if (hasChanges === true && this.OnFilterRefresh !== undefined && this.OnFilterRefresh !== null) {
            this.OnFilterRefresh(this.GetFilterData());
        }
    },

    setFilterButtonStates: function () {
        var hasChanges = false;
        var isEmpty = true;
        var self = this;
        this.Fields.forEach(function (field) {
            var inputForField = $('#' + self.ContentId + ' input[filterKey="' + field.Key + '"]');
            var selectForField = $('#' + self.ContentId + ' select[filterKey="' + field.Key + '"]');
            if (inputForField.length === 1 && inputForField.val() !== field.Value) {
                hasChanges = true;
            }
            if (selectForField.length === 1 && selectForField.val() !== field.Value) {
                hasChanges = true;
            }
            if (field.Value !== null && field.Value !== '') {
                isEmpty = false;
            }
        });
        $('#' + this.ContentId + ' button#applyFilterButton').prop('disabled', !hasChanges);
        $('#' + this.ContentId + ' button#clearFilterButton').prop('disabled', isEmpty);
    }
}

function FilterField(description, key, fieldType, filterType, getItemValue, valuesList) {
    this.Description = description;
    this.Key = key;
    this.FieldType = fieldType;
    this.FilterType = filterType;
    this.Value = '';
    this.GetItemValue = getItemValue;
    this.ValuesList = valuesList;
}

FilterField.prototype = {
    IsMatchFilter: function (item) {
        //Check for undefined.
        if (this.GetItemValue === null || this.GetItemValue === undefined ||
            this.Value === null || this.Value === undefined) {
            return true;
        }

        //Check for valid criteria.
        if (this.FieldType === FieldTypeEnum.Text && (this.FilterType === FilterTypeEnum.GreaterOrEquals || this.FilterType === FilterTypeEnum.LessOrEquals)) {
            return true;
        }
        if (this.FieldType !== FieldTypeEnum.Text && this.FilterType === FilterTypeEnum.Contains) {
            return true;
        }

        //Get "filter by" value based on type.
        var filterByValue = this.Value;
        if (filterByValue === null || filterByValue === '') {
            return true;
        }
        var itemValue = this.GetItemValue(item);
        switch (this.FieldType) {
            case FieldTypeEnum.Date:
                try {
                    filterByValue = this.stringToDate(filterByValue);
                    itemValue = this.stringToDate(itemValue);
                } catch (err) {
                    return true;
                }
                break;
            case FieldTypeEnum.Number:
                filterByValue = Number(filterByValue);
                itemValue = Number(itemValue);
                if (filterByValue === NaN || itemValue === NaN) {
                    return true;
                }
                break;
            default:
                break;
        }

        //Try to compare values.
        switch (this.FilterType) {
            case FilterTypeEnum.Equals:
                return itemValue === filterByValue;
            case FilterTypeEnum.Contains:
                return itemValue.indexOf(filterByValue) >= 0;
            case FilterTypeEnum.GreaterOrEquals:
                return itemValue >= filterByValue;
            case FilterTypeEnum.LessOrEquals:
                return itemValue <= filterByValue;
            default:
                return true;
        }
    },

    stringToDate: function(data) {
        var date = data.split('-');
        return new Date(date[0], date[1] - 1, date[2]);
    }
}
