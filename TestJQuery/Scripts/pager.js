﻿function Pager(pageSize, pagerContainerId, goToPrevPageCommand, goToNextPageCommand) {
    this.PageSize = pageSize;
    this.PagerContainerId = pagerContainerId;
    this.CurrentPage = 1;
    this.GoToPrevPageCommand = goToPrevPageCommand;
    this.GoToNextPageCommand = goToNextPageCommand;

    this.buildPager();
}

Pager.prototype = {
    RefreshPageNumber: function () {
        $('#' + this.PagerContainerId + ' > span#pageNumber').text(this.CurrentPage.toString());
    },

    SetStateForControls: function (isEnabled) {
        $('#' + this.PagerContainerId + ' > button').prop("disabled", isEnabled === false);
        $('#' + this.PagerContainerId + ' > button#prevButton').prop("disabled", isEnabled === false || this.CurrentPage === 1);
    },

    buildPager: function () {
        $('#' + this.PagerContainerId).append(
            jQuery('<button />').addClass('btn btn-default').prop('id', 'prevButton').text('Prev').on('click', this.GoToPrevPageCommand));
        $('#' + this.PagerContainerId).append(jQuery('<span />').prop('id', 'pageNumber'));
        $('#' + this.PagerContainerId).append(
            jQuery('<button />').addClass('btn btn-default').prop('id', 'nextButton').text('Next').on('click', this.GoToNextPageCommand));

        this.RefreshPageNumber();
        this.SetStateForControls();
    }
}