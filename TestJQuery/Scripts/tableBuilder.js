﻿function TableBuilder(contentId, tableClass, columns, onEditButtonClick, editColumnClass) {
    this.ContentId = contentId;
    this.Columns = columns;
    this.OnEditButtonClick = onEditButtonClick;
    this.EditColumnClass = editColumnClass;
    this.storedContent = null;

    var table = jQuery('<table />').addClass('table table-hover').addClass(tableClass);
    table.append(jQuery('<thead />'));
    table.append(jQuery('<tbody />'));
    $('#' + this.ContentId).append(table);
    this.buildHeader();
}

TableBuilder.prototype = {
    BuildContent: function (data) {
        var table = $('#' + this.ContentId + ' > table > tbody');
        table.html('');
        var self = this;
        data.forEach(function (dataItem) {
            table.append(self.createRow(dataItem, dataItem[0]));
        });
        this.storedContent = null;
    },

    WriteMessageToContent: function (message) {
        this.storedContent = $('#' + this.ContentId + ' > table > tbody').html();
        $('#' + this.ContentId + ' > table > tbody').html(message);
    },

    RestoreContent: function () {
        if (this.storedContent !== null && this.storedContent != undefined) {
            $('#' + this.ContentId + ' > table > tbody').html(this.storedContent);
            this.storedContent = null;
        }
    },

    WriteMessageToRowContent: function (message, id) {
        var row = $('#' + this.ContentId).find('#row_' + id);
        if (row.length === 1) {
            row.html(message);
        }
    },

    ReloadRow: function (values, id) {
        var row = $('#' + this.ContentId).find('#row_' + id);
        if (row.length === 1) {
            row.html('');
            this.createRowContent(row, values, id);
        }
    },

    SetStateForControls: function (isEnabled) {
        $('#' + this.ContentId + ' > table > tbody > tr > td > button').prop("disabled", isEnabled === false);
    },

    buildHeader: function () {
        var header = $('#' + this.ContentId + ' > table > thead');
        header.html('');
        header.append(this.createRow(undefined, "header", true));
    },

    createRowContent: function (rowToAppend, values, id, isHeader) {
        var cellTag = '<td />';
        if (isHeader === true) {
            cellTag = '<th />';
        }
        this.Columns.forEach(function (item, index) {
            var text = item.Text;
            if (values !== undefined && values.length > index) {
                text = values[index];
            }
            var newCell = jQuery(cellTag).addClass(item.ClassName);
            newCell.text(text);
            rowToAppend.append(newCell);
        });
        if (values !== undefined && this.OnEditButtonClick !== undefined) {
            rowToAppend.append(jQuery(cellTag).addClass(this.EditColumnClass).append(jQuery('<button />').addClass('btn btn-default').text('Edit').attr('onclick', this.OnEditButtonClick + '(' + id + ')')));
        }
        if (values === undefined && this.OnEditButtonClick !== undefined) {
            rowToAppend.append(jQuery(cellTag).addClass(this.EditColumnClass));
        }
    },

    createRow: function (values, id, isHeader) {
        var result = jQuery('<tr />').prop('id', 'row_' + id);
        this.createRowContent(result, values, id, isHeader);
        return result;
    }
}


function Column(text, className) {
    this.Text = text;
    if (className === null || className === undefined) {
        this.ClassName = 'col' + text;
    } else {
        this.ClassName = className;
    }
}