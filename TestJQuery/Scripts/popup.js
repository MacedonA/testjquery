﻿function Popup(containerId, getViewUrl) {
    this.ContainerId = containerId;
    this.GetViewUrl = getViewUrl;
    this.DoAfterSubmit = null;

    this.form = null;
    this.buildPopup();
}

Popup.prototype = {
    Show: function (title, id, doAfterSubmit, width, height) {
        this.setWindowSize(width, height);
        var self = this;
        this.Id = id;
        this.DoAfterSubmit = doAfterSubmit;
        $('#' + this.ContainerId + ' div#popupTitle').append(jQuery('<label />').text(title));
        this.showErrors('');
        $('#' + this.ContainerId + ' div#popupDataContent').html('...loading');
        $('#' + this.ContainerId + ' div#popupButtons > button').prop('disabled', true);
        $('#' + this.ContainerId).show();
        var url = this.GetViewUrl;
        if (id !== null && id !== undefined) {
            url = url + '?id=' + id;
        }
        $.ajax({
            cache: false,
            type: 'GET',
            url: url,
            success: function (data) {
                if (data.Error !== undefined) {
                    self.Close();
                    alert(data.Error);
                    console.log(data.Error);
                } else {
                    $('#' + self.ContainerId + ' div#popupDataContent').html(data);
                    self.form = $('#' + self.ContainerId + ' div#popupDataContent form');
                    self.form.removeData('validator');
                    self.form.removeData('unobtrusiveValidation');
                    $.validator.unobtrusive.parse(self.form);
                    $('#' + self.ContainerId + ' div#popupButtons > button').prop('disabled', false);
                }
            },
            error: function (data) {
                self.Close();
                alert('Error!!!!');
                console.log(data);
            }
        });
    },

    Close: function () {
        this.form = null;
        $('#' + this.ContainerId).hide();
        this.showErrors('');
        $('#' + this.ContainerId + ' div#popupTitle').html('');
    },

    buildPopup: function () {
        var self = this;
        var popupWindow = jQuery('<div />').addClass('popupWindow');
        popupWindow.append(jQuery('<div />').prop('id', 'popupTitle'));
        var loadableContent = jQuery('<div />').prop('id', 'popupDataContainer');
        loadableContent.append(jQuery('<div />').prop('id', 'popupErrors'));
        loadableContent.append(jQuery('<div />').prop('id', 'popupDataContent'));
        popupWindow.append(loadableContent);
        popupWindow.append(
            jQuery('<div />').prop('id', 'popupButtons')
                .append(jQuery('<button />').on('click', function () { self.submit(); }).addClass('btn btn-default').text('OK'))
                .append(jQuery('<button />').on('click', function () { self.Close(); }).addClass('btn btn-default').text('Cancel')));
        $('#' + this.ContainerId).addClass('popupContainer').append(popupWindow);
        this.setWindowSize();
    },

    submit: function () {
        this.showErrors('');
        if (this.form === null || this.form === undefined) {
            return;
        }
        var validator = this.form.validate();
        if (!this.form.valid()) {
            this.showErrors('', validator.errorMap);
            return;
        }
        var self = this;
        var serializedData = this.form.serializeArray();

        $.ajax({
            cache: false,
            type: this.form.prop('method'),
            url: this.form.prop('action'),
            data: serializedData,
            success: function (data) {
                if (data.Errors === undefined) {
                    self.Close();
                    if (self.DoAfterSubmit !== undefined && self.DoAfterSubmit !== null) {
                        self.DoAfterSubmit(self.Id, serializedData);
                    }
                } else {
                    var validator = self.form.validate();
                    self.showErrors('', data.Errors, validator);
                }
            },
            error: function (data) {
                console.log(data);
                self.showErrors('Unexpected error.');
            }
        });
    },

    showErrors: function (errorMessage, errorMap, validator) {
        var errorsList = jQuery('<ul />').addClass('popupErrorsList');
        if (errorMap !== null && errorMap !== undefined) {
            for (var key in errorMap) {
                if (Array.isArray(errorMap[key])) {
                    errorMap[key].forEach(function (error) {
                        errorsList.append(jQuery('<li />').text(error));
                    });
                } else {
                    errorsList.append(jQuery('<li />').text(errorMap[key]));
                }
            }
            if (validator !== null && validator !== undefined) {
                validator.showErrors(errorMap);
            }
        }
        $('#' + this.ContainerId + ' div#popupErrors').html('');
        if (errorMessage !== null && errorMessage !== undefined && errorMessage !== '') {
            $('#' + this.ContainerId + ' div#popupErrors').append(jQuery('<span />').text(errorMessage));
        }
        $('#' + this.ContainerId + ' div#popupErrors').append(errorsList);
    },

    setWindowSize: function (width, height) {
        if (!Number.isInteger(width) || width < 300) {
            width = 300;
        }
        if (!Number.isInteger(height) || height < 300) {
            height = 300;
        }
        $('#' + this.ContainerId + ' div.popupWindow').css('width', width + 'px');
        $('#' + this.ContainerId + ' div.popupWindow').css('margin-left', (-width / 2).toString() + 'px');
        $('#' + this.ContainerId + ' div.popupWindow').css('height', height + 'px');
        $('#' + this.ContainerId + ' div.popupWindow').css('margin-top', (-height/2).toString() + 'px');

        $('#' + this.ContainerId + ' div#popupTitle').css('height', '35px');

        $('#' + this.ContainerId + ' div#popupDataContainer').css('height', (height - 75).toString() + 'px');

        $('#' + this.ContainerId + ' div#popupButtons').css('height', '40px');
    }
}