﻿using System;

namespace TestJQuery.Models
{
    public class CarDto
    {
        public int Id { get; set; }

        public string Manufacturer { get; set; }

        public string Model { get; set; }

        public double Engine { get; set; }

        public int Year { get; set; }

        public DateTime DeliveredOn { get; set; }
    }
}