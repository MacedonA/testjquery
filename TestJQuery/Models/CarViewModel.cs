﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace TestJQuery.Models
{
    public class CarViewModel
    {
        [Required]
        public int Id { get; set; }

        [Required]
        [MaxLength(50)]
        public string Manufacturer { get; set; }

        public IList<string> AvailableManufacturers { get; set; }

        [Required]
        [MaxLength(50)]
        public string Model { get; set; }

        [Required]
        [Range(0.2, 10.0)]
        public double Engine { get; set; }

        [Required]
        [Range(1900, 2200)]
        public int Year { get; set; }

        [DisplayName("Delivered")]
        public DateTime DeliveredOn { get; set; }
    }
}