﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using TestJQuery.Filter;
using TestJQuery.Reposirtory.Models;

namespace TestJQuery.Reposirtory
{
    public class CarRepository
    {
        public IList<CarModel> GetList(int count, int skip, CarFilter filter)
        {
            Thread.Sleep(100);
            if (skip < 0 || count <= 0)
                return new List<CarModel>();
            Thread.Sleep(900);
            return DatabaseSimulator.GetAllData(filter).Skip(skip).Take(count).ToList();
        }

        public CarModel GetItem(int id)
        {
            Thread.Sleep(1000);
            return DatabaseSimulator.GetItem(id);
        }

        public int AddCar(CarModel car)
        {
            Thread.Sleep(200);
            return DatabaseSimulator.AddItem(car);
        }

        public bool EditCar(CarModel car)
        {
            Thread.Sleep(200);
            return DatabaseSimulator.EditItem(car);
        }
    }
}