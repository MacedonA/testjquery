﻿using System;
using System.Collections.Generic;
using System.Linq;
using TestJQuery.Filter;
using TestJQuery.Reposirtory.Models;

namespace TestJQuery.Reposirtory
{
    /// <summary>
    /// Just DB emulation.
    /// </summary>
    public static class DatabaseSimulator
    {
        private static readonly List<CarModel> Data = new List<CarModel>
        {
            new CarModel { Id = 1, DeliveredOn = new DateTime(2017, 1, 1, 0, 0, 0, DateTimeKind.Utc), Engine = 2.0, Manufacturer = "Nissan", Model = "Juke", Year = 2015 },
            new CarModel { Id = 2, DeliveredOn = new DateTime(2017, 1, 5, 0, 0, 0, DateTimeKind.Utc), Engine = 1.6, Manufacturer = "Peugeot", Model = "3008", Year = 2016 },
            new CarModel { Id = 3, DeliveredOn = new DateTime(2017, 2, 5, 0, 0, 0, DateTimeKind.Utc), Engine = 1.8, Manufacturer = "Mazda", Model = "CX-5", Year = 2016 },
            new CarModel { Id = 4, DeliveredOn = new DateTime(2017, 3, 5, 0, 0, 0, DateTimeKind.Utc), Engine = 1.4, Manufacturer = "BMW", Model = "X3", Year = 2016 },
            new CarModel { Id = 5, DeliveredOn = new DateTime(2017, 4, 5, 0, 0, 0, DateTimeKind.Utc), Engine = 1.2, Manufacturer = "Kia", Model = "Sportage", Year = 2016 },
            new CarModel { Id = 6, DeliveredOn = new DateTime(2017, 5, 5, 0, 0, 0, DateTimeKind.Utc), Engine = 2.2, Manufacturer = "Citroen", Model = "C5", Year = 2016 },
            new CarModel { Id = 7, DeliveredOn = new DateTime(2017, 6, 5, 0, 0, 0, DateTimeKind.Utc), Engine = 2.7, Manufacturer = "Lada", Model = "Granta", Year = 2017 },
            new CarModel { Id = 8, DeliveredOn = new DateTime(2017, 7, 5, 0, 0, 0, DateTimeKind.Utc), Engine = 1.6, Manufacturer = "BMW", Model = "X5", Year = 2017 },
            new CarModel { Id = 9, DeliveredOn = new DateTime(2017, 8, 5, 0, 0, 0, DateTimeKind.Utc), Engine = 1.2, Manufacturer = "Toyota", Model = "RAV 4", Year = 2017 },
            new CarModel { Id = 10, DeliveredOn = new DateTime(2017, 9, 5, 0, 0, 0, DateTimeKind.Utc), Engine = 1.4, Manufacturer = "Hynday", Model = "Tuscon", Year = 2017 },
        };

        public static List<CarModel> GetAllData(CarFilter filter)
        {
            return Data.Where(d => filter?.IsMatch(d) != false).Select(d => d.Copy()).ToList();
        }

        public static CarModel GetItem(int id)
        {
            return Data.FirstOrDefault(d => d.Id == id)?.Copy();
        }

        public static int AddItem(CarModel car)
        {
            var toAdd = car.Copy();
            toAdd.Id = Data.Any() ? Data.Max(d => d.Id) + 1 : 1;
            Data.Add(toAdd);
            return toAdd.Id;
        }

        public static bool EditItem(CarModel car)
        {
            var toEdit = Data.FirstOrDefault(d => d.Id == car.Id);
            if (toEdit == null)
                return false;
            toEdit.Update(car);
            return true;
        }

        public static IList<string> GetAvailableManufacturers()
        {
            return new List<string>
            {
                "Nissan",
                "Peugeot",
                "Mazda",
                "BMW",
                "Kia",
                "Citroen",
                "Lada",
                "Toyota",
                "Hynday"
            };
        }
    }
}