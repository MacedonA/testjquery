﻿using System;
using TestJQuery.Filter;

namespace TestJQuery.Reposirtory.Models
{
    public class CarModel
    {
        public int Id { get; set; }

        [FilterableField("filterManufacturer")]
        public string Manufacturer { get; set; }

        [FilterableField("filterModel")]
        public string Model { get; set; }

        public double Engine { get; set; }

        [FilterableField("filterYear")]
        public int Year { get; set; }

        [FilterableField("filterDeliveredBefore")]
        [FilterableField("filterDeliveredAfter")]
        public DateTime DeliveredOn { get; set; }

        public CarModel Copy()
        {
            var result = new CarModel { Id = Id };
            result.Update(this);
            return result;
        }

        public void Update(CarModel car)
        {
            DeliveredOn = car.DeliveredOn;
            Engine = car.Engine;
            Manufacturer = car.Manufacturer;
            Model = car.Model;
            Year = car.Year;
        }
    }
}