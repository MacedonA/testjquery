﻿using System.Linq;
using System.Web.Mvc;
using TestJQuery.Reposirtory;

namespace TestJQuery.Controllers
{
    public class ManufacturerController : Controller
    {
        [HttpGet]
        public ActionResult LoadManufacturers()
        {
            return Json(new ManufacturerRepository().GetList().Select(m => m.Clone()).ToList(), JsonRequestBehavior.AllowGet);
        }
    }
}
