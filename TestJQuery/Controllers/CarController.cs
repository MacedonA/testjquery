﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TestJQuery.Filter;
using TestJQuery.Models;
using TestJQuery.Reposirtory;
using TestJQuery.Reposirtory.Models;

namespace TestJQuery.Controllers
{
    public class CarController : Controller
    {
        [HttpGet]
        public ActionResult LoadCars(int count, int skip)
        {
            var searchResult = GetCarsFromRepository(count, skip, null).ToArray();
            if (!searchResult.Any())
                return Json(new { Message = "No data." }, JsonRequestBehavior.AllowGet);
            return Json(searchResult.Select(c =>
                new CarDto
                {
                    Model = c.Model,
                    Manufacturer = c.Manufacturer,
                    Year = c.Year,
                    Engine = c.Engine,
                    Id = c.Id,
                    DeliveredOn = c.DeliveredOn
                }).ToList(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult LoadCars(int count, int skip, CarFilter filter)
        {
            var searchResult = GetCarsFromRepository(count, skip, filter).ToArray();
            if (!searchResult.Any())
                return Json(new { Message = "No data." }, JsonRequestBehavior.AllowGet);
            return Json(searchResult.Select(c =>
                new CarDto
                {
                    Model = c.Model,
                    Manufacturer = c.Manufacturer,
                    Year = c.Year,
                    Engine = c.Engine,
                    Id = c.Id,
                    DeliveredOn = c.DeliveredOn
                }).ToList(), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetOne(int id)
        {
            var car = new CarRepository().GetItem(id);
            if (car == null)
                return Json(new { Error = "Car was not found." });
            return Json(
                new CarDto
                {
                    Model = car.Model,
                    Manufacturer = car.Manufacturer,
                    Year = car.Year,
                    Engine = car.Engine,
                    Id = car.Id,
                    DeliveredOn = car.DeliveredOn
                }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult CarEditDialog(int? id)
        {
            CarViewModel viewModel;
            if (id.HasValue)
            {
                var car = (new CarRepository()).GetItem(id.Value);
                if (car == null)
                    return Json(new {Error = "Car was not found."});
                viewModel = new CarViewModel
                {
                    Model = car.Model,
                    Manufacturer = car.Manufacturer,
                    Year = car.Year,
                    Engine = car.Engine,
                    Id = car.Id,
                    DeliveredOn = car.DeliveredOn
                };
            }
            else
            {
                viewModel = new CarViewModel
                {
                    Id = 0,
                    Year = DateTime.Today.Year,
                    DeliveredOn = DateTime.Today
                };
            }
            var manufacturers = new List<string> { null };
            manufacturers.AddRange((new ManufacturerRepository()).GetList().OrderBy(i => i));
            viewModel.AvailableManufacturers = manufacturers;
            return View("CarEditDialog", viewModel);
        }

        [HttpPost]
        public ActionResult CarEditDialog(CarViewModel car)
        {
            if (car.Year > DateTime.Today.Year)
            {
                ModelState.AddModelError(nameof(car.Year), "Too large for me.");
                ModelState.AddModelError(nameof(car.Year), "Be careful.");
            }
            if (!ModelState.IsValid)
            {
                var keys = ModelState.Keys.Where(k => ModelState[k].Errors.Any());
                return Json(new
                {
                    Errors = keys.ToDictionary(k => k, k => ModelState[k].Errors.Select(e => e.ErrorMessage).ToList())
                });
            }

            //Add a new car.
            if (car.Id <= 0)
            {
                var isValidAdd = (new CarRepository()).AddCar(new CarModel
                {
                    Model = car.Model,
                    Manufacturer = car.Manufacturer,
                    Year = car.Year,
                    Engine = car.Engine,
                    Id = car.Id,
                    DeliveredOn = car.DeliveredOn
                });
                return isValidAdd > 0 ? Json("Valid!!!") : Json(new { Errors = new[] { "Something wrong" } });
            }

            //Add a new car.
            var isValidEdit = (new CarRepository()).EditCar(new CarModel
            {
                Model = car.Model,
                Manufacturer = car.Manufacturer,
                Year = car.Year,
                Engine = car.Engine,
                Id = car.Id,
                DeliveredOn = car.DeliveredOn
            });
            return isValidEdit ? Json("Valid!!!") : Json(new { Errors = "Car was not found." });
        }

        private IEnumerable<CarModel> GetCarsFromRepository(int count, int skip, CarFilter filter)
        {
            return new CarRepository().GetList(count, skip, filter);
        }
    }
}
